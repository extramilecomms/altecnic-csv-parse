# Instructions for parsing the attributes 

## CSV Setup
1. Ask the client to add the custom attributes into individual columns at the end of the Magento import template which is shared with them
2. When complete the custom attribute columns need to be extracted from the rest of the columns and placed into their own CSV
3. Use the **parse_csv.php** script to create the **additional_attributes** column required to import into Magento

## Creating the additional_attributes column
1. Add the custom attributes CSV created in step 2 above to the *input* folder
1. Update the file path in the **parse_csv.php** file
1. run `php parse_csv.php` from the CLI to create the combined attributes file
1. Open the file
1. copy the additional_attributes column
1. paste the column into the main import file and remove the custom attribute columns (these ar no longer required)

## Run the import


## Common Issues
- Ensure all of the attribute values are already in the Magento system
- Ensure price column is filled
- Do not select field enclosure in the admin before importing 
- Check attribute references match the ones in Magento
