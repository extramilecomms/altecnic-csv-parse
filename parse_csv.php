<?php

$row = 1;
$attribute_codes = [];
$csv_ready_array = [];
$input_file = '200921-altecnic-attributes.csv';

if( ($handle = fopen(__DIR__ . "/input/" . $input_file, "r") ) !== FALSE ) {
    // Loop over each row in the CSV
    while(($row_data = fgetcsv($handle, 1000, ",")) !== FALSE ) {
        $num = count($row_data);
        $additional_attributes = [];
        $attributes_combined = '';

        // store the column headers when on the first row
        if( $row == 1) {
            for( $c=0; $c < $num; $c++ ) {
                array_push($attribute_codes, $row_data[$c]);
            }
            array_push($attribute_codes, "additional_attributes");
            $csv_ready_array[] = $attribute_codes;
        }
        
        // build the attribute column for all other rows
        if( $row > 1) {
            // loop over each column and build the attibutes string
            // atribute_code=value
            for( $c=0; $c < $num; $c++ ) {
                // check the column has a value
                // add to string
                if( !empty($row_data[$c]) ) {
                    if( $attributes_combined !== '') {
                        $attributes_combined .= ",";
                    }
                    $attributes_combined .= $attribute_codes[$c] . '=' . filter_attr_value($row_data[$c]);
                }
            }
            
            // add the $attributes_combined var to the CSV row data
            $row_data[] = $attributes_combined;
            // add the row data to the array for CSV output
            $csv_ready_array[] = $row_data;
        }


        // if ($row == 3) {
        //     print_r($csv_ready_array);
        //     exit;
        // }

        $row++;
        
    }

    fclose($handle);

    createCSV( $csv_ready_array );

}

function filter_attr_value( $attr_value ) {

    // replace "," with a "|"
    $regex = '/(,\s?)/';
    $filtered_attr_value = preg_replace( $regex, "|", $attr_value );

    return $filtered_attr_value;
}

function createCSV( $data ) {
    $fp = fopen( __DIR__ . "/output/altecnic_attributes_combined.csv", 'w' );
    foreach( $data as $row) {
        fputcsv( $fp, $row);
    }
    fclose( $fp );
}